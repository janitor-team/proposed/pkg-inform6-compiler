# debian/tests/smoke-compile.sh
# Part of Debian ‘inform6-compiler’ package.
#
# This is free software, and you are welcome to redistribute it under
# certain conditions; see the end of this file for copyright
# information, grant of license, and disclaimer of warranty.

# Common functionality for compiler smoke tests.

document_source_file="${DOCUMENT_NAME}".inf
document_source_path="debian/tests/${document_source_file}"
document_file="${DOCUMENT_NAME}"${DOCUMENT_SUFFIX}
document_path="debian/tests/${document_file}"

function compile_document() {
    inform6 ${INFORM_OPTS} ${document_source_path} ${document_path}
}

function assert_file_type() {
    local expected_type_pattern="$1"

    local expected_output_pattern="^${document_path}: ${expected_type_pattern}$"

    file_type="$(file "${document_path}")"
    printf "File type output: “%s”\n" "${file_type}"
    printf "Expected pattern: “%s”\n" "${expected_output_pattern}"

    grep -q "${expected_output_pattern}" <(printf "%s\n" "${file_type}")
}


# Copyright © 2016–2022 Ben Finney <bignose@debian.org>
# This is free software; you may copy, modify, and/or distribute this
# work under the terms of the GNU General Public License as published
# by the Free Software Foundation; version 3 of that License or later.
# No warranty expressed or implied.

# Local variables:
# coding: utf-8
# mode: shell-script
# End:
# vim: fileencoding=utf-8 filetype=sh :
